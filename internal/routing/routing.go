package routing

import (
	"github.com/gofiber/fiber/v2"
)

func Index(c *fiber.Ctx) error {
	return c.Status(200).SendString("Hello darkness ")
}

func GetIp(c *fiber.Ctx) error {
	return c.Status(200).SendString(c.IP())
}
