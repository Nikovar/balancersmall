package main

import (
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/limiter"
	"gitlab.com/Nikovar/balancersmall/internal/routing"
)

func main() {
	app := fiber.New(fiber.Config{
		BodyLimit:    64 * 1024 * 1024,
		ServerHeader: "Balance",
		AppName:      "Balance",
	})
	app.Server().MaxConnsPerIP = 4
	app.Server().Name = "Balancer"
	app.Use(limiter.New(limiter.Config{
		Max:        20,
		Expiration: 30 * time.Second,
	}))
	app.Server().StreamRequestBody = true

	app.Static("/static", "./files")
	app.Get("/", routing.Index)

	app.Get("/GetIp", routing.GetIp)
}
